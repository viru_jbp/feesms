package com.example.feesms.endpoint;

import com.example.feesms.domain.Fees;
import com.example.feesms.repo.FeesRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class FeesResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeesResource.class);
    @Autowired
    FeesRepo feesRepo;

    @GetMapping("/fees")
    public List<Fees> getAllStudentsFees(){
        LOGGER.info("Fetching all student fees from DB");
        return feesRepo.findAll();
    }

    @PostMapping("fees")
    public ResponseEntity<Fees> addStudentFees(@RequestBody Fees fees) throws URISyntaxException {
        LOGGER.info("Saving Student Fees");
        fees.setId(null);
        Fees studentFees = feesRepo.save(fees);
        return ResponseEntity.created(new URI(studentFees.getStudentFees().toString())).body(studentFees);
    }

    @GetMapping("/hello")
    public String getHello() {
        System.out.println("****************");
        return "Hello World!";
    }

}
