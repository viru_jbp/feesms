package com.example.feesms.repo;

import com.example.feesms.domain.Fees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeesRepo extends JpaRepository<Fees, Integer> {

}
