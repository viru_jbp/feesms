package com.example.feesms.domain;

import javax.persistence.*;

@Entity
@Table(name="fees_data")
public class Fees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer studentId;
    private Integer studentFees;

    public Integer getId() {
        return id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public Integer getStudentFees() {
        return studentFees;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public void setStudentFees(Integer studentFees) {
        this.studentFees = studentFees;
    }
}
